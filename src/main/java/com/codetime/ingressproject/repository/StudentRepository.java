package com.codetime.ingressproject.repository;

import com.codetime.ingressproject.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
