package com.codetime.ingressproject.repository;

import com.codetime.ingressproject.domain.Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupsRepository extends JpaRepository<Group, Long> {
}
