package com.codetime.ingressproject.repository;

import com.codetime.ingressproject.domain.StudentDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentDetailsRepository extends JpaRepository<StudentDetails, Long> {
}
