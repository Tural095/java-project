package com.codetime.ingressproject.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@Table(name = "students")
@EqualsAndHashCode(exclude = "group")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    private String email;

    private Integer age;

    @OneToOne(mappedBy = "student", cascade = CascadeType.PERSIST)
    @PrimaryKeyJoinColumn
    private StudentDetails studentDetails;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    private Group group;

}
