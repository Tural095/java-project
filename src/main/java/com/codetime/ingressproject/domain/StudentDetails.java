package com.codetime.ingressproject.domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@Table(name = "student_details")
@ToString(exclude = "student")
public class StudentDetails {

    @Id
    @Column(name = "student_id")
    private Long id;

    private String about;

    @OneToOne
    @MapsId
    @JoinColumn(name = "student_id")
    private Student student;

}
