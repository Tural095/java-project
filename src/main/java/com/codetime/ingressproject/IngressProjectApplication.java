package com.codetime.ingressproject;

import com.codetime.ingressproject.domain.Account;
import com.codetime.ingressproject.domain.Group;
import com.codetime.ingressproject.domain.Student;
import com.codetime.ingressproject.domain.StudentDetails;
import com.codetime.ingressproject.repository.AccountRepository;
import com.codetime.ingressproject.repository.GroupsRepository;
import com.codetime.ingressproject.repository.StudentDetailsRepository;
import com.codetime.ingressproject.repository.StudentRepository;
import com.codetime.ingressproject.services.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManagerFactory;
import java.util.Set;

@RequiredArgsConstructor
@SpringBootApplication
public class IngressProjectApplication implements CommandLineRunner {

    private final StudentRepository studentRepository;
    private final StudentDetailsRepository studentDetailsRepository;
    private final GroupsRepository groupsRepository;
    private final TransferService transferService;
    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;

    public static void main(String[] args) {
        SpringApplication.run(IngressProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Account account1 = new Account();
        account1.setName("Ayxan");
        account1.setBalance(200.0);

        accountRepository.save(account1);

        Account account2 = new Account();
        account2.setName("Rufat");
        account2.setBalance(200.0);

        accountRepository.save(account2);

        Account ayko = accountRepository.findByName(account1.getName()).get();
        Account rufo = accountRepository.findByName(account2.getName()).get();

        //transferService.transferTransactional(20.0);

        //transferService.transfer(ayko, rufo, 20.0);


        //OneToMany -ManyToOne
//        Student student = new Student();
//        student.setName("Ruslan");
//        student.setAge(36);
//        student.setEmail("ruslan@ukr.com");
//
//        Student student1 = new Student();
//        student1.setName("Alyosh");
//        student1.setAge(26);
//        student1.setEmail("alivaria@test.com");
//
//        Group group = new Group();
//        group.setName("JAVA");
//
//        student.setGroup(group);
//        student1.setGroup(group);
//
//        group.setStudents(Set.of(student, student1));
//
//        groupsRepository.save(group);

        // OneToOne
//		Student student = new Student();
//		student.setName("Geray");
//		student.setEmail("geray95@karimli.az");
//		student.setAge(9);
//
//
//		StudentDetails studentDetails = new StudentDetails();
//		studentDetails.setAbout("test");
//
//		student.setStudentDetails(studentDetails);
//		studentDetails.setStudent(student);
//
//
//		studentRepository.save(student);

    }
}
