package com.codetime.ingressproject.services;

import com.codetime.ingressproject.domain.Account;
import com.codetime.ingressproject.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import static java.lang.Thread.sleep;

@RequiredArgsConstructor
@Service
public class TransferService {

   // private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public void transferTransactional(Double amount) throws Exception {
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            Account source = entityManager.find(Account.class,13L);
            Account target = entityManager.find(Account.class,14L);
            transfer(source, target, amount);
        } catch (RuntimeException e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.getTransaction().commit();
            entityManager.close();
        }
    }


    @SneakyThrows
    @Transactional
    public void transfer(Account source, Account target, Double amount) throws Exception {
        if (source.getBalance() < amount) {
            throw new RuntimeException("insufficient balance");
        }
        source.setBalance(source.getBalance() - amount);
        target.setBalance(target.getBalance() + amount);

        //sleep(10000);
        //accountRepository.save(source);
        entityManager.persist(source);
        if (true)
            throw new Exception("database connection failed");
        //accountRepository.save(target);
        entityManager.persist(target);


    }
}
